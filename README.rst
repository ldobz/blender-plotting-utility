==========================
 Blender Plotting Utility
==========================
---------------------------------------
 Addon for making shaded scatter plots
---------------------------------------

Addon Provides functionality for generating charts from json data.

--------------
 Installation
--------------

***********************
 Supported Environment
***********************
Blender 2.73

********************************************
 Installing Addon to blender using terminal
********************************************

 - Clone plotting utility repository.
 - Copy "plotting_utility.py" into blender addons folder (Using blender 2.75 this is in "/home/luke/.config/blender/2.75/scripts/addons")
 - From the command line, start the blender server in the background using "blender --background --python run.py"