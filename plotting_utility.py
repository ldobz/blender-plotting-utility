""" 
    Blender Data Plotting Utility
    Copyright (C) 2015  Luke Ashley Dobinson

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

    contact via email: ldobinson90@gmail.com
"""

bl_info = {"name": "Plotting Utility", "category": "Object"}

import os.path
import uuid
import http.server
import urllib.parse
import json
import logging

# code for producing plots and returning them
class PlotterHTTPRequestHandler(http.server.BaseHTTPRequestHandler):
    output_path = '/tmp/blender_plotting_renders'

    def do_GET(self):
        logging.info("Handling GET request...")

        urlstr = urllib.parse.urlparse(self.path)
        qs = urllib.parse.parse_qs(urlstr.query)

        if not ('json' in qs):
            logging.warning("Rejecting request: no json querystring argument")
            self.send_bad_response('No "json" querystring argument')
            return

        try:
            data = json.loads(qs['json'][0])
        except ValueError as err:
            logging.warning("Rejecting request: bad json syntax")
            self.send_bad_response("Invalid json syntax: {0}".format(err))
            return

        try:
            self.plot_data(data)
        except ValueError as err:
            logging.warning("Error plotting data: {0}".format(err))
            self.send_bad_response("Problem Plotting data")
            return

        full_output_path = self.render()
        self.send_image_response(full_output_path)

    def send_image_response(self, full_output_path):
        """Writes the rendered image"""
        self.send_response(200)
        self.send_header('Content-Type', 'image/png')
        self.end_headers()
        # write image here
        f = open(full_output_path, 'rb')
        self.wfile.write(f.read())
        f.close()

    def send_bad_response(self, msg, encoding='UTF-8'):
        """Writes an error message response."""
        self.send_response(400)
        self.send_header('Content-Type', 'text/plain')
        self.end_headers()
        self.wfile.write(bytes(msg, encoding))

    def plot_data(self, data):
        """Plots the data to the temporary output file and returns its location."""
        clear_scene()

        model = PlotModel(data)

        # set the rendering options for the scene
        model.get_render_options().set_render_options()

        plotted_series = []
        for data, plotter in model.get_data_to_plotters().items():
            plot_series = plotter.plot(data)
            plotted_series.append(plot_series)

        axes = model.get_axes()
        axes.plot(plotted_series)

    def render(self):
        """Returns the rendered output as an image in memory."""
        file_id = uuid.uuid4()

        image_output_path = os.path.join(self.output_path, "{0}.png".format(file_id))
        bpy.context.scene.render.filepath = image_output_path

        bpy.ops.render.render(write_still=True)

        blend_output_path = os.path.join(self.output_path, "{0}.blend".format(file_id))
        bpy.ops.wm.save_as_mainfile(filepath=blend_output_path)

        return image_output_path

import bpy

# code for starting the server
class RunPlotter(bpy.types.Operator):
    bl_idname = "object.runplotter"
    bl_label = "Run Plotter"
    bl_options = {"REGISTER", "UNDO"}

    address = bpy.props.StringProperty(name="Server Address", default="")
    port = bpy.props.IntProperty(name="Port", default=8000)

    def execute(self, context):
        """Starts the HTTP request handler which listens for incoming plots."""
        server_address = (self.address, self.port)
        httpd = http.server.HTTPServer(server_address, PlotterHTTPRequestHandler)

        logging.info("Starting server...")
        httpd.serve_forever()
        return { "FINISHED" }

def register():
    bpy.utils.register_class(RunPlotter)

def unregister():
    bpy.utils.unregister_class(RunPlotter)

def clear_scene(): 
    set_mode_object()

    for obj in bpy.context.scene.objects:
        obj.select = True
        bpy.ops.object.delete()

if __name__ == "__main__":
    register()

#===============================RENDER OPTIONS=============================
class RenderOptions:
    """sets up global render options which apply to the entire plot"""
    
    cycle_counts = {
        'preview': 1,
        'normal': 20,
        'detailed': 100
    }

    def __init__(self, data):
        self.bg_color = color_from_rgb(data["background_color"])
        self.cycle_count = self.cycle_counts[data["quality"]]

    def set_render_options(self):
        bpy.context.scene.render.engine = 'CYCLES'
        bpy.context.scene.world.horizon_color = self.bg_color
        bpy.context.scene.render.resolution_percentage = 100
        bpy.context.scene.cycles.samples = self.cycle_count

#===============================PLOT MODEL=================================
class PlotModel:
    '''Validates the plot data by converting it to a model class'''

    def __init__(self, data):
        self.validate_keys(data, { "plots", "axes_options", "render_options" })

        self.data_to_plotters = { }

        for plot in data["plots"]:
            plot_type = plot["plot_type"]
            plot_options = plot["plot_options"]
            plot_data_columns = plot["plot_data"]["columns"]
            plot_data_rows = plot["plot_data"]["rows"]

            plot_data = RowConverter(plot_data_columns).get_rows(plot_data_rows)
            plot_plotter = self.parse_plotter(plot_type, plot_options)
            self.data_to_plotters[plot_data] = plot_plotter

        self.axes = Axes(data["axes_options"])
        self.render_options = RenderOptions(data["render_options"])

    def validate_keys(self, dict, expected_keys):
        '''raises an error if the dictionary does not have the expected keys'''

        actual_args = set(dict.keys())
        missing_args = expected_keys - actual_args
        superfluous_args = actual_args - expected_keys

        if missing_args or superfluous_args:
            raise ValueError("{0}{1}".format(
                "The arguments {0} must be provided.".format(missing_args) if missing_args else "",
                "\nThe arguments {0} are invalid.".format(superfluous_args) if superfluous_args else ""
                ))

    def parse_plotter(self, plot_type, options):
        if plot_type == "scatter":
            return ScatterPlotter(options)

        raise ValueError('The plot type "{0}" is not supported.'.format(plot_type))

    def get_data_to_plotters(self):
        return self.data_to_plotters

    def get_axes(self):
        return self.axes

    def get_render_options(self):
        return self.render_options

import mathutils

class RowConverter:
    '''converts a list of data ranges into a set of positions, bounding boxes, and rotations'''

    valid_column_names = ('x0', 'y0', 'z0', 'x1', 'y1', 'z1', 'x2', 'y2', 'z2')
    
    def __init__(self, columns):
        '''defines the columns used by this importer and the indexes where they 
        are expected to appear'''

        for column_name in columns:
            if not column_name in RowConverter.valid_column_names:
                raise ValueError('"{0}" is not a valid column name. Valid column names are: {1}.'.format(column_name, RowConverter.valid_column_names))

            if columns[column_name] < 0:
                raise ValueError('Column indices must be non-negative.')

        self.columns = columns

    def get_rows(self, rows):
        '''generates the plot data'''

        for row in rows:
            if len(row) != len(self.columns):
                raise ValueError("Row data must match the dimensionality of the" +
                    "declared columns. Length provided: {0} (Required: {1})".format(row,self.columns))

            x0 = row[self.columns['x0']] if 'x0' in self.columns else None
            x1 = row[self.columns['x1']] if 'x1' in self.columns else None
            x2 = row[self.columns['x2']] if 'x2' in self.columns else None
            x0, x1, x2 = self.fill_missing_values(x0, x1, x2)

            y0 = row[self.columns['y0']] if 'y0' in self.columns else None
            y1 = row[self.columns['y1']] if 'y1' in self.columns else None
            y2 = row[self.columns['y2']] if 'y2' in self.columns else None
            y0, y1, y2 = self.fill_missing_values(y0, y1, y2)

            z0 = row[self.columns['z0']] if 'z0' in self.columns else None
            z1 = row[self.columns['z1']] if 'z1' in self.columns else None
            z2 = row[self.columns['z2']] if 'z2' in self.columns else None
            z0, z1, z2 = self.fill_missing_values(z0, z1, z2)

            yield {
                'position': mathutils.Vector((x1, y1, z1)),
                'dimensions': {
                    'x': { 'min': x0 - x1, 'max': x2 - x1 },
                    'y': { 'min': y0 - y1, 'max': y2 - y1 },
                    'z': { 'min': z0 - z1, 'max': z2 - z1 }
                    },
                'rotation': { 'x': 0, 'y': 0, 'z': 0 }
            }

    def fill_missing_values(self, x0, x1, x2):
        '''adds in any range data which is missing'''
        if x0 == None and x1 == None and x2 == None:
            return (0 , 0, 0)

        x0 = self.defer_if_null(x0, x1, x2)
        x2 = self.defer_if_null(x2, x1, x0)
        x1 = self.defer_if_null(x1, x0, x2)

        if x0 > x1 or x1 > x2:
            raise ValueError("Invalid Range: {0}".format((x0, x1, x2)))

        return (x0, x1, x2)

    def defer_if_null(self, *args):
        """Returns the first argument which is not None. Returns None if all args are None."""
        if len(args) == 0:
            raise ValueError("Must provide at least one argument to defer to.")
        for arg in args:
            if arg != None: return arg

        return None

#===============================PLOTTER=================================
class ScatterPlotter:
    """Scatter Plots an object"""
    bl_idname = "object.scatter_plotter"
    bl_label = "Scatter Plotter"
    bl_options = { 'REGISTER', 'UNDO' }

    def __init__(self, options):
        valid_point_types = {
            'diamond': self.create_diamond
        }

        if not options["point_type"] in valid_point_types.keys():
            raise ValueError('"{0}" is not a valid point type, valid point types are {1}'.format(
                options["point_type"],
                tuple(valid_point_types.keys())
                ))

        self.plot_function = valid_point_types[options["point_type"]]
        
        self.material = bpy.data.materials.new(name="Scatter_Plot")
        self.material.diffuse_color = color_from_rgb(options["color"])

        if options["wireframe"]:
            self.wireframe = True
            self.wireframe_material = bpy.data.materials.new(name="Scatter_Plot wireframe")
            self.wireframe_material.diffuse_color = (0, 0, 0)

    def plot(self, plot_data):
        """plots the data series, and returns the plot parent object"""

        bpy.ops.object.empty_add(type='PLAIN_AXES', location=(0, 0, 0))
        plot_parent_object = bpy.context.object
        plot_parent_object.name = "Scatter Plot"

        for row in plot_data:
            self.plot_function(row["position"], 
                                            row["dimensions"],
                                            row["rotation"],
                                            plot_parent_object,
                                            self.material)
            if self.wireframe:
                self.plot_function(row["position"], row["dimensions"], row["rotation"], plot_parent_object, self.wireframe_material, True)

        return plot_parent_object

    def create_diamond(self, position, dimensions, rotation, plot_parent_object, material, wireframe=False):
        '''creates the scaled plot point'''
        set_mode_object()

        me = bpy.data.meshes.new("plot_point_mesh")
        ob = bpy.data.objects.new("plot_point {0}".format(position), me)
        bpy.context.scene.objects.link(ob)
        ob.data.materials.append(material)

        verts = [
            (dimensions['x']['min'], 0, 0),
            (0, dimensions['y']['max'], 0),
            (dimensions['x']['max'], 0, 0),
            (0, dimensions['y']['min'], 0),
            (0, 0, dimensions['z']['max']),
            (0, 0, dimensions['z']['min'])
            ]
        faces = [
            (0, 4, 1),
            (1, 4, 2),
            (0, 3, 4),
            (4, 3, 2),
            (1, 0, 5),
            (1, 2, 5),
            (2, 3, 5),
            (3, 0, 5)
        ]

        me.from_pydata(verts, [], faces)

        bpy.context.scene.objects.active = ob

        if bpy.context.scene.objects.active != ob:
            raise ValueError("Error occurred here.")

        ob.location += position
        ob.rotation_mode = 'XYZ'
        ob.rotation_euler = mathutils.Euler((rotation['x'], rotation['y'], rotation['z']), 'XYZ')

        if wireframe:
            bpy.ops.object.modifier_add(type='WIREFRAME')
            bpy.context.object.modifiers["Wireframe"].thickness = 0.2

        if plot_parent_object:
            ob.parent = plot_parent_object

#===============================AXES=================================
import math

class Ticks:
    """Defines a single set of ticks for an axis"""
    def __init__(self, data, rng):
        if math.fabs(data["offset"]) >= math.fabs(data["interval"]):
            raise ValueError("offset must be less than interval")

        if rng["max"] < rng["min"]:
            raise ValueError("maximum value must be greater than minimum value")

        self.offset = data["offset"]
        self.interval = data["interval"]
        self.max = rng["max"]
        self.min = rng["min"]

    def get_tic_coordinates(self):
        """Yields the tic coordinates. Applies the specified scaling if provided"""

        # start from offset and count down
        current_value = self.min + self.offset
        while current_value <= self.max:
            yield current_value
            current_value += self.interval

class Axis:
    """Defines a single axis"""
    def __init__(self, orientation, data):
        if not orientation in { 'x', 'y', 'z' }:
            raise ValueError("Axis orientation must be x, y, or z")

        if data["range"]["min"] >= data["range"]["max"]:
            raise ValueError("Invalid range, maximum must be greater than minimum value")

        if data["scale"] <= 0:
            raise ValueError("Scale must be postitive and non-zero")

        self.orientation = orientation
        self.label = data["label"] if "label" in data else ""
        self.range = data["range"]
        self.min = data["range"]["min"]
        self.max = data["range"]["max"]
        self.minor_ticks = Ticks(data["minor_ticks"], self.range) if "minor_ticks" in data else None
        self.major_ticks = Ticks(data["major_ticks"], self.range) if "major_ticks" in data else None
        self.labels = Ticks(data["labels"], self.range) if "labels" in data else None
        self.labels_format_code = data["labels"]["format_code"] if "labels" in data else None
        self.scale = data["scale"]

    def scaled_major_tick_coordinates(self):
        for x in self.major_ticks.get_tic_coordinates(): yield x * self.scale

    def scaled_minor_tick_coordinates(self):
        for x in self.minor_ticks.get_tic_coordinates(): yield x * self.scale

    def scaled_label_coordinates(self):
        for x in self.labels.get_tic_coordinates(): yield x * self.scale

    def scaled_bounds(self):
        for x in self.range.values(): yield x * self.scale

class Axes:
    """Defines the entire chart, with all of the axes"""
    def __init__(self, data):
        self.x_axis = Axis("x", data["x"]) if "x" in data else None
        self.y_axis = Axis("y", data["y"]) if "y" in data else None
        self.z_axis = Axis("z", data["z"]) if "z" in data else None

        if data["gridlines"]["xy"] and not (self.x_axis or self.y_axis):
            raise ValueError("XY gridlines specified but x or y axis missing.")

        if data["gridlines"]["xz"] and not (self.x_axis or self.z_axis):
            raise ValueError("XZ gridlines specified but x or z axis missing.")

        if data["gridlines"]["yz"] and not (self.y_axis or self.z_axis):
            raise ValueError("YZ gridlines specified but y or z axis missing.")

        self.xy_gridlines = data["gridlines"]["xy"]
        self.xz_gridlines = data["gridlines"]["xz"]
        self.yz_gridlines = data["gridlines"]["yz"]

        # add the circle used to bevel the chart
        self.axes_bevel = self.draw_bezier_circle("axes bevel", 0.03)
        self.minor_bevel = self.draw_bezier_circle("tick minor bevel", 0.03)

        self.material = bpy.data.materials.new(name="Scatter_Plot")
        self.material.diffuse_color = (0, 0, 0)

        bpy.ops.object.empty_add(type='PLAIN_AXES', location=(0, 0, 0))
        self.axes_parent_object = bpy.context.object
        self.axes_parent_object.name = "Axes"
        
        self.camera_rotation = data["camera"]["rotation"]

        self.x_resolution = data["camera"]["resolution"]["x"]
        self.y_resolution = data["camera"]["resolution"]["y"]

        # rescale all axes so scales are relative
        total_scale = self.x_axis.scale if self.x_axis else 0 + \
                      self.y_axis.scale if self.y_axis else 0 + \
                      self.z_axis.scale if self.z_axis else 0

        diagonal_range = (1 / 40) * math.sqrt(self.get_range('x')**2 + self.get_range('y')**2 + self.get_range('z')**2)

        if self.x_axis:
            self.x_axis.scale /= total_scale
            self.x_axis.scale /= diagonal_range

        if self.y_axis:
            self.y_axis.scale /= total_scale
            self.y_axis.scale /= diagonal_range

        if self.z_axis:
            self.z_axis.scale /= total_scale
            self.z_axis.scale /= diagonal_range

        axes_center = (
            self.get_min('x', apply_scale=True) + self.get_range('x', apply_scale=True) / 2,
            self.get_min('y', apply_scale=True) + self.get_range('y', apply_scale=True) / 2,
            self.get_min('z', apply_scale=True) + self.get_range('z', apply_scale=True) / 2
            )
    
        bpy.ops.object.empty_add(type='PLAIN_AXES', location=axes_center)
        self.axes_center_object = bpy.context.object
        self.axes_center_object.name = "Axes Center Point"
        bpy.context.object.parent = self.axes_parent_object


    def get_axis_from_name(self, name):
        if name == 'x':
            return self.x_axis if self.x_axis else None
        elif name == 'y':
            return self.y_axis if self.y_axis else None
        elif name == 'z':
            return self.z_axis if self.z_axis else None

        raise ValueError("{0} is not a valid axis name, valid names are (x, y, z)".format(name))

    def get_min(self, name, apply_scale=False):
        axis = self.get_axis_from_name(name)
        if not axis: return 0

        return axis.min * (axis.scale if apply_scale else 1)

    def get_max(self, name, apply_scale=False):
        axis = self.get_axis_from_name(name)
        if not axis: return 0

        return axis.max * (axis.scale if apply_scale else 1)

    def get_range(self, name, apply_scale=False):
        return self.get_max(name, apply_scale) - self.get_min(name, apply_scale)

    def draw_bezier_circle(self, name, radius):
        set_mode_object()
        bpy.ops.curve.primitive_bezier_circle_add(location=(0,0,0), radius = radius)
        bpy.context.object.name = name
        return bpy.context.object

    def draw_line(self, start, stop, bevel_obj=None, parent_obj=None):
        """draws a single straight line between the two vectors and bevels it"""
        me = bpy.data.meshes.new("line_mesh")
        ob = bpy.data.objects.new("line", me)
        verts = [start, stop]
        edges = [[0, 1]]
        me.from_pydata(verts, edges, [])
        bpy.context.scene.objects.link(ob)
        ob.select = True
        bpy.context.scene.objects.active = ob
        bpy.ops.object.convert(target='CURVE')
        if bevel_obj:
            bpy.context.object.data.bevel_object = bevel_obj

        if parent_obj:
            bpy.context.object.parent = parent_obj

        bpy.context.object.data.materials.append(self.material)

    def plot(self, plot_objects):
        """draws the axes and plot objects"""
        self.draw_major_lines()
        self.draw_minor_ticks()

        # Resize the plotted objects. Due to the use of the wireframe modifier, 
        # may need to apply scaling that has been carried out on the data series.
        scale_vector = (self.x_axis.scale if self.x_axis else 1, 
                        self.y_axis.scale if self.y_axis else 1, 
                        self.z_axis.scale if self.z_axis else 1)

        for plot_object in plot_objects:
            plot_object.scale = scale_vector

            for plot_child_object in plot_object.children:
                bpy.context.scene.objects.active = plot_child_object
                bpy.context.object.select = True
                bpy.ops.object.parent_clear(type='CLEAR_KEEP_TRANSFORM')
                bpy.ops.object.transform_apply(location=False, rotation=True, scale=True)

        camera = self.create_camera()
        self.draw_labels(point_at_object=camera)

    def draw_major_lines(self):
        # bookmark add scaling to ticks here
        x_min = self.get_min('x', apply_scale=True)
        y_min = self.get_min('y', apply_scale=True)
        z_min = self.get_min('z', apply_scale=True)
        x_max = self.get_max('x', apply_scale=True)
        y_max = self.get_max('y', apply_scale=True)
        z_max = self.get_max('z', apply_scale=True)

        # Add the bounding box to each set of ticks
        if self.x_axis:
            x_ticks = list(self.x_axis.scaled_major_tick_coordinates())
            x_bounds = list(self.x_axis.scaled_bounds())
        
        if self.y_axis:
            y_ticks = list(self.y_axis.scaled_major_tick_coordinates())
            y_bounds = list(self.y_axis.scaled_bounds())
        
        if self.z_axis:
            z_ticks = list(self.z_axis.scaled_major_tick_coordinates())
            z_bounds = list(self.z_axis.scaled_bounds())

        if not (self.xy_gridlines or self.xz_gridlines):
            if self.x_axis:
                start = (x_min, y_min, z_min)
                stop = (x_max, y_min, z_min)
                self.draw_line(start, stop, self.axes_bevel, self.axes_parent_object)

        if not (self.xy_gridlines or self.yz_gridlines):
            if self.y_axis:
                start = (x_min, y_min, z_min)
                stop = (x_min, y_max, z_min)
                self.draw_line(start, stop, self.axes_bevel, self.axes_parent_object)

        if not (self.xz_gridlines or self.yz_gridlines):
            if self.z_axis:
                start = (x_min, y_min, x_min)
                stop = (x_min, y_min, z_max)
                self.draw_line(start, stop, self.axes_bevel, self.axes_parent_object)

        if self.xy_gridlines:
            for y in set(y_ticks + y_bounds):
                start = (x_min, y, z_min)
                stop = (x_max, y, z_min)
                self.draw_line(start, stop, self.axes_bevel, self.axes_parent_object)
                
            for y in y_ticks:
                start = (x_max, y, z_min)
                stop = (x_max + 1, y, z_min)
                self.draw_line(start, stop, self.axes_bevel, self.axes_parent_object)

            for x in set(x_ticks + x_bounds):
                start = (x, y_min, z_min)
                stop = (x, y_max, z_min)
                self.draw_line(start, stop, self.axes_bevel, self.axes_parent_object)

            for x in x_ticks:
                start = (x, y_max, z_min)
                stop = (x, y_max + 1, z_min)
                self.draw_line(start, stop, self.axes_bevel, self.axes_parent_object)

        if self.xz_gridlines:
            for z in set(z_ticks + z_bounds):
                start = (x_min, y_min, z)
                stop = (x_max, y_min, z)
                self.draw_line(start, stop, self.axes_bevel, self.axes_parent_object)
            for x in set(x_ticks + x_bounds):
                start = (x, y_min, z_min)
                stop = (x, y_min, z_max)
                self.draw_line(start, stop, self.axes_bevel, self.axes_parent_object)

        if self.yz_gridlines:
            for z in set(z_ticks + z_bounds):
                start = (x_min, y_min, z)
                stop = (x_min, y_max, z)
                self.draw_line(start, stop, self.axes_bevel, self.axes_parent_object)
            for y in set(y_ticks + y_bounds):
                start = (x_min, y, z_min)
                stop = (x_min, y, z_max)
                self.draw_line(start, stop, self.axes_bevel, self.axes_parent_object)

    def draw_minor_ticks(self):
        x_min = self.get_min('x', apply_scale=True)
        y_min = self.get_min('y', apply_scale=True)
        z_min = self.get_min('z', apply_scale=True)
        x_max = self.get_max('x', apply_scale=True)
        y_max = self.get_max('y', apply_scale=True)
        z_max = self.get_max('z', apply_scale=True)

        if self.x_axis and self.x_axis.minor_ticks:
            for x in self.x_axis.scaled_minor_tick_coordinates():
                start = (x , y_max, z_min)
                stop = (x, y_max + 0.5, z_min)
                self.draw_line(start, stop, self.minor_bevel, self.axes_parent_object)

        if self.y_axis and self.y_axis.minor_ticks:
            for y in self.y_axis.scaled_minor_tick_coordinates():
                start = (x_max, y, z_min)
                stop = (x_max + 0.5, y, z_min)
                self.draw_line(start, stop, self.minor_bevel, self.axes_parent_object)

        if self.z_axis and self.z_axis.minor_ticks:
            for z in self.z_axis.scaled_minor_tick_coordinates():
                start = (x_max, y_min, z)
                stop = (x_max + 0.35355339059, y_min - 0.35355339059, z)
                self.draw_line(start, stop, self.minor_bevel, self.axes_parent_object)

    def draw_labels(self, point_at_object):
        """draws a set of text labels, aligned to the view of the point_at object"""
        if self.x_axis and self.x_axis.labels:
            for scaled_coord, real_coord in zip(self.x_axis.scaled_label_coordinates(), self.x_axis.labels.get_tic_coordinates()):
                text = self.x_axis.labels_format_code % real_coord
                location = (scaled_coord, self.get_max('y', apply_scale=True) + 1.5, self.get_min('z', apply_scale=True) - 1)

                self.draw_text(text, location, point_at_object, self.axes_parent_object, self.material)
                
        if self.y_axis and self.y_axis.labels:
            for scaled_coord, real_coord in zip(self.y_axis.scaled_label_coordinates(), self.y_axis.labels.get_tic_coordinates()):
                text = self.y_axis.labels_format_code % real_coord
                location = (self.get_max('x', apply_scale=True) + 1.5, scaled_coord, self.get_min('z', apply_scale=True) - 1)

                self.draw_text(text, location, point_at_object, self.axes_parent_object, self.material)

        if self.z_axis and self.z_axis.labels:
            for scaled_coord, real_coord in zip(self.z_axis.scaled_label_coordinates(), self.z_axis.labels.get_tic_coordinates()):
                text = self.z_axis.labels_format_code % real_coord
                location = (self.get_max('x', apply_scale=True) + 1.5, self.get_min('y', apply_scale=True), scaled_coord)
                self.draw_text(text, location, point_at_object, self.axes_parent_object, self.material)

    def draw_text(self, text, location, orient_towards, parent_to, material):
        """Draws a text object at the location. Orients it towards the orient_towards object and makes its parent the parent_to object."""
        bpy.ops.object.text_add()
        text_object = bpy.context.object
        text_object.data.body = text
        bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_MASS')
        
        bpy.ops.object.constraint_add(type='COPY_ROTATION')
        text_object.constraints["Copy Rotation"].target = orient_towards

        text_object.location = location
        text_object.parent = parent_to
        text_object.data.materials.append(material)

    # creates a camera which looks at the specified rotation
    def create_camera(self):
        # ensure the camera is out of the chart area
        camera_distance = math.hypot(
            self.get_range('x', apply_scale=True), 
            math.hypot(self.get_range('y', apply_scale=True), self.get_range('z', apply_scale=True)))

        relative_camera_location = (
            camera_distance * math.cos(self.camera_rotation["z"]) * math.sin(self.camera_rotation["x"]),
            camera_distance * math.sin(self.camera_rotation["z"]) * math.sin(self.camera_rotation["x"]),
            camera_distance * math.cos(self.camera_rotation["x"])
            )

        #calculate location based on axes_center_object and parent to it
        bpy.ops.object.camera_add(location = relative_camera_location)
        camera = bpy.context.object
        bpy.context.scene.camera = camera
        camera.data.type = "ORTHO"
        camera.data.ortho_scale = camera_distance + 3
        camera.parent = self.axes_center_object

        bpy.context.scene.render.pixel_aspect_x = self.x_resolution
        bpy.context.scene.render.pixel_aspect_y = self.y_resolution
        bpy.context.scene.render.resolution_x = self.x_resolution
        bpy.context.scene.render.resolution_y = self.y_resolution

        # track to the center of the chart
        bpy.ops.object.constraint_add(type='TRACK_TO')
        camera.constraints["Track To"].target = self.axes_center_object
        camera.constraints["Track To"].track_axis = 'TRACK_NEGATIVE_Z'
        camera.constraints["Track To"].up_axis = 'UP_Y'

        return camera

#===============================HELPERS=================================
def set_mode_object():
    if bpy.ops.object.mode_set.poll():
            bpy.ops.object.mode_set(mode='OBJECT')

def set_mode_edit():
    if bpy.ops.object.mode_set.poll():
            bpy.ops.object.mode_set(mode='EDIT')

def color_from_rgb(rgb_dict):
    if set(rgb_dict.keys()) != { "red", "green", "blue" }:
        raise ValueError("Color data must contain only Red, Green, and Blue values.")

    if any(x < 0 or x > 1 for x in rgb_dict.values()):
        raise ValueError("Background colors must be floating point numbers in the range 0 to 1")

    return (rgb_dict["red"], rgb_dict["green"], rgb_dict["blue"])
